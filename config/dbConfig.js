const mysql = require('mysql')
module.exports = {
    //数据库配置
    config:{
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: 'mysql',
        database: 'testdb',
    },
    //链接数据库，使用连接池
    sqlConnect:function(sql, sqlArr, callBack){
        console.log("链接数据库")
        const pool = mysql.createPool(this.config)
        pool.getConnection((err, conn) => {
            if(err){
                console.log("链接出错");
                return;
            }
            //时间驱动回调
            conn.query(sql, sqlArr, callBack);
            //释放链接
            conn.release();
        })
    }
}