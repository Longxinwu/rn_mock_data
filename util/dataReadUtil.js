const fs = require("fs");

fileExist = (reqPath) =>{
    console.log("123"+reqPath);
    fs.access(reqPath, fs.constants.F_OK, err => {
        if(err){
            console.log("不存在");
            return false;
        }else {
            return true;
        }
    })
}
readFile=(filePath)=>{
    let data = fs.readFileSync(filePath, 'utf-8');
    data = JSON.parse(data);
    console.log(data);
    return data;
}
module.exports={
    fileExist,
    readFile
}