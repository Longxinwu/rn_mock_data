const dbConfig = require("../config/dbConfig");
getUser = (req, res) => {
    console.log("获取数据");
    let sql = "select * from rn_data";
    let sqlArr = [];
    callBackFunc(sql, sqlArr,req,res);
}
addUser = (req,res) => {
    let obj = req.body;
    let name = obj.name;
    let pass = obj.pass;
    let sql = "INSERT into rn_data (user_name,user_pass) select ?, ? from DUAL where not EXISTS (SELECT user_name from rn_data where user_name = ?)"
    let params = [name, pass,name];
    callBackFunc(sql, params, req, res, "用户添加成功");
}

userLogin = (req, res) => {
    let obj = req.body;
    let name = obj.userName;
    let pass = obj.userPass;

    let sql = "select user_pass from rn_data where user_name = ?"
    let sqlArr = [name];
    let respData = {
        data:{
            userName:name,
        },
        respCode:"00",
        respMsg:"登录成功"
    }
    if(name === "admin1"){
        if(pass === "123456"){
            res.send(respData);
        }else{
            respData.respCode = "01";
            respData.respMsg = "用户密码错误";
            res.send(respData);
        }
    }else{
        respData.data = {};
        respData.respCode = "02";
        respData.respMsg = "用户不存在";
        res.send(respData);
    }
    /*const callBack = (err, data) => {
        console.log("getData"+data.toString());
        if(err){
            console.log("获取数据失败");
        }else{
            if(!data){
                let respData = {
                    respCode:"02",
                    respMsg:"用户不存在"
                }
                res.send(respData);
            }else{
                let respData = {
                    data:{
                        userName:name,
                    },
                    respCode:"00",
                    respMsg:"登录成功"
                }
                let str = data[0].user_pass;
                if(pass === str){
                    res.send(respData);
                }else{
                    respData.respCode = "01";
                    respData.respMsg = "用户密码错误";
                    res.send(respData);
                }
            }
        }
    }*/
    //dbConfig.sqlConnect(sql, sqlArr,callBack)

}


function callBackFunc(sql, sqlArr, req, res, resStr){
    const callBack = (err,data)=>{
        if(err){
            console.log("获取数据失败");
        }else {
            console.log(data);
            res.send(
                constructResult(data, resStr),
            );
        }
    }
    dbConfig.sqlConnect(sql, sqlArr, callBack);
}
function constructResult(data, resStr){
    let respData = {
        params: "",
        respCode: "00",
        respMsg: "success",
    }
    if(resStr != null && resStr != ""){
        respData.params = resStr;
    }else{
        respData.params = data;
    }
    return respData;
}
module.exports = {
    getUser,
    addUser,
    userLogin,
}