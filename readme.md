# node+express搭建基本的api调试服务器
## 环境准备
### 安装node
安装node这里不进行展开，请自行查询网上其他教程
### 安装express
1. 全局安装：npm install express -g
2. npm install -g express-generator 
3. cmd中输入一下命令示如下图所示即表示环境准备完成
![img.png](img.png)
###创建项目
```shell
express rn_mock_data
```
###安装数据库
```shell
npm install mysql --save
```

#数据库准备
这里创建了一个rn_data数据库表在testdb数据库中
```sql
##用户表
DROP TABLE IF EXISTS `rn_data`;
create table `rn_data` (
	id int(11) not null AUTO_INCREMENT,
	user_name varchar(50) not null COMMENT '用户名',
	user_pass varchar(255) not null COMMENT '密码',
	role varchar(50) default null ,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into `rn_data` (user_name, user_pass, role) values('admin1', '123456','admin');
insert into `rn_data` (user_name, user_pass, role) values('admin2', '654321','admin');
```