const express = require('express');
const router = express.Router();
const path = require('path');
const DIR = 'data';
const testControl = require('../controller/testController');
const url = require('url');
const dataRead = require("../util/dataReadUtil");

/* GET home page. */
router.get('/user', testControl.getUser);
router.post('/addUser', testControl.addUser);
router.post('/api/userLogin', testControl.userLogin);

/*router.post('/readFile', function (req,res){
    const pathName = url.parse(req.url).pathname;
    console.log("名字"+pathName);
    const realPath = path.join(DIR, pathName);
    if(dataRead.fileExist(realPath)){
       const data =  dataRead.readFile(realPath);
       console.log("读取数据");
    }
});*/


module.exports = router;
